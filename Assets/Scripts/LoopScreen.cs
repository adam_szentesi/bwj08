using UnityEngine;

public class LoopScreen : MonoBehaviour
{
  [SerializeField]
  private Renderer Renderer;

  private Material ScreenMaterial;

  private void Awake()
  {
    ScreenMaterial = Renderer.sharedMaterial;
    SetTextureOffset(Vector2.zero);
  }

  public void SetTextureOffset(Vector2 offset)
  {
    ScreenMaterial.SetTextureOffset("_MainTex", offset);
  }

  public void SetTextureScale(Vector2 scale)
  {
    ScreenMaterial.SetTextureScale("_MainTex", scale);
  }

  public void SetScale(float x, float y)
  {
    transform.localScale = new Vector3(x, y, 1);
  }

  public void SetPosition(Vector2 position)
  {
    transform.localPosition = new Vector3(position.x, position.y, transform.localPosition.z);
  }

}
