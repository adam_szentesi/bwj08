using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/BlackRealm")]
public class BlackRealm : Realm
{
  public override void OnAction()
  {
    Debug.Log("Black action");
  }

  public override void OnMove(Direction direction)
  {
    Game.Instance.MovePlayer(direction);
  }

}
