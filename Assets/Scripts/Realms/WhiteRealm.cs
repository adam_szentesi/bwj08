using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/WhiteRealm")]
public class WhiteRealm : Realm
{
  public override void OnAction()
  {
    Debug.Log("White action");
  }

  public override void OnMove(Direction direction)
  {
    Game.Instance.MovePlayer(direction);
  }

}
