using UnityEngine;

public class Player : Tileable
{
  public void SetPosition(Vector2 position)
  {
    transform.localPosition = position;
  }

}
