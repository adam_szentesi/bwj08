using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditor : MonoBehaviour
{
  [SerializeField]
  private GameObject CursorPrefab;

  private Cursor Cursor;

  private void Awake()
  {
    Cursor = Instantiate(CursorPrefab).GetComponent<Cursor>();
  }

}
