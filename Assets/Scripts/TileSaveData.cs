using System;
using UnityEngine;

[Serializable]
public struct TileSaveData
{
    public Vector2Int TilePosition;
    public TileType TileType;

    public TileSaveData(Vector2Int tilePosition, TileType tileType)
    {
        TilePosition = tilePosition;
        TileType = tileType;
    }

}
