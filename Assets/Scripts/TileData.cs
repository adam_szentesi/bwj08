using System;

[Serializable]
public struct TileData
{
    public TileLogic TileLogic { get; private set; }

    public TileType TileType => TileLogic ? TileLogic.TileType : TileType.None;
    public bool CanMoveTo() => TileLogic ? TileLogic.CanMoveTo() : false;

    public TileData(TileLogic tileLogic)
    {
        TileLogic = tileLogic;
    }

}
