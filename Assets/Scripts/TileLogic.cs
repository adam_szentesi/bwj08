using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/TileLogic")]
public class TileLogic : ScriptableObject
{
  [SerializeField]
  private TileType _TileType;
  public TileType TileType => _TileType;

  [SerializeField]
  private Sprite _TileSprite;
  public Sprite TileSprite => _TileSprite;

  public virtual bool CanMoveTo()
  {
    return false;
  }

}
