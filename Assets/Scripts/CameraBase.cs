using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBase : MonoBehaviour
{
  public float Size => Camera.orthographicSize;

  protected Camera Camera { get; private set; }

  protected virtual void Awake()
  {
      Camera = GetComponent<Camera>();
  }

  public void SetCameraSize(float size)
  {
      Camera.orthographicSize = size;
  }

  public void SetPosition(Vector2 position)
  {
      transform.localPosition = new Vector3(position.x, position.y, transform.localPosition.z);
  }

}
