using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class TileManager : MonoBehaviour
{
  public int Width { get; private set; }
  public int Height { get; private set; }

  [SerializeField]
  private GameObject TilePrefab;

  [SerializeField]
  private Transform TilesParent;

  [SerializeField]
  private TileLogic InvalidTileLogic;

  [SerializeField]
  private TileLogic[] TileLogicSetups = new TileLogic[0];

  private Dictionary<TileType, TileLogic> TileLogics = new Dictionary<TileType, TileLogic>();
  private Tile[,] Tiles;

  private void Awake()
  {
    TileLogics.Add(TileType.None, InvalidTileLogic);

    foreach (TileLogic tileLogic in TileLogicSetups)
    {
      if (tileLogic != null && !TileLogics.ContainsKey(tileLogic.TileType))
      {
        TileLogics.Add(tileLogic.TileType, tileLogic);
      }
    }
  }

  public Sprite GetTileSprite(TileType tileType)
  {
    return GetTileLogic(tileType).TileSprite;
  }

  public TileLogic GetTileLogic(TileType tileType)
  {
    return TileLogics[tileType];
  }

  public void LoadLevel(LevelData levelData)
  {
    // OPTI: pool these instead of destroying them
    if (Tiles != null)
    {
      foreach (Tile tile in Tiles)
      {
        tile.Selfdestruct();
      }
    }

    ReadOnlyCollection<TileSaveData> tileSaveDatas = levelData.GetData();

    Width = levelData.Size.width;
    Height = levelData.Size.height;

    Tiles = new Tile[Width, Height];

    for (int x = 0; x < Width; x++)
    {
      for (int y = 0; y < Height; y++)
      {
        Tile tile = Instantiate(TilePrefab, TilesParent).GetComponent<Tile>();
        tile.Init(new Vector3Int(x, y, 0));
        Tiles[x, y] = tile;
      }
    }

    foreach (TileSaveData tileSaveData in tileSaveDatas)
    {
      Tiles[tileSaveData.TilePosition.x, tileSaveData.TilePosition.y].SetTileLogic(GetTileLogic(tileSaveData.TileType));
    }
  }

  public bool CanMoveTo(Vector2Int tilePosition)
  {
    Tile tile = GetTile(tilePosition);

    return tile ? tile.CanMoveTo() : false;
  }

  private bool IsWithinTiles(Vector2Int gridPosition)
  {
    return IsWithinTiles(gridPosition.x, gridPosition.y);
  }

  private bool IsWithinTiles(int x, int y)
  {
    return !(x < 0 || x >= Width || y < 0 || y >= Height);
  }

  private Tile GetTile(Vector2Int gridPosition)
  {
    return GetTile(gridPosition.x, gridPosition.y);
  }

  private Tile GetTile(int x, int y)
  {
    if(IsWithinTiles(x, y))
    {
      return Tiles[x, y];
    }

    return null;
  }

  public void ToggleTiles(bool state)
  {
    foreach (Tile tile in Tiles)
    {
      tile.Toggle(state);
    }
  }

  public void ToggleTileUnsafe(int x, int y, bool state)
  {
    Tiles[x, y].Toggle(state);
  }

}

