using UnityEngine;

public class Tileable : MonoBehaviour
{
  public Vector3Int TilePosition { get; private set; } = Vector3Int.zero;
  public Vector2Int TilePosition2D => new Vector2Int(TilePosition.x, TilePosition.y);

  protected void InitInternal(Vector3Int tilePosition)
  {
    SetTilePosition(tilePosition);
  }

  public void SetTilePosition(Vector3Int tilePosition)
  {
    TilePosition = tilePosition;
    transform.localPosition = TilePosition;
  }

  public void SetTilePosition(Vector2Int tilePosition)
  {
    SetTilePosition(new Vector3Int(tilePosition.x, tilePosition.y, TilePosition.z));
  }

  public void SetPosition(Vector2 position)
  {
    transform.localPosition = position;
  }

}
