using System;
using UnityEngine;

public abstract class Realm : ScriptableObject
{
  public Color Color;
  public int LoopRadius = 3;

  public abstract void OnAction();
  public virtual void OnMove(Direction direction) { }

}
