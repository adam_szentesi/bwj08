using UnityEngine;

public enum TileType
{
  None,
  Empty,
  Rock,
}

public class Tile : Tileable
{
  [SerializeField]
  private SpriteRenderer Renderer;

  public TileLogic TileLogic { get; private set; }

  public TileType TileType => TileLogic ? TileLogic.TileType : TileType.None;
  public bool CanMoveTo() => TileLogic ? TileLogic.CanMoveTo() : false;

  public void Init(Vector3Int tilePosition, TileLogic tileLogic = null)
  {
    name = "Tile" + tilePosition;
    InitInternal(tilePosition);
    SetTileLogic(tileLogic);
  }

  public void SetTileLogic(TileLogic tileLogic)
  {
    TileLogic = tileLogic;
    SetTileSprite(Game.GetTileSprite(TileType));
  }

  public void SetTileSprite(Sprite tileSprite)
  {
    Renderer.sprite = tileSprite;
  }

  public void SetAlpha(float alpha)
  {
    Color color = Renderer.color;
    color.a = alpha;
    Renderer.color = color;
  }

  public void Toggle(bool state)
  {
    gameObject.SetActive(state);
  }

  public void Selfdestruct()
  {
    Destroy(gameObject);
  }

}
