using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/Palette")]
public class Palette : ScriptableObject
{
  public List<Realm> Realms = new List<Realm>();

}
