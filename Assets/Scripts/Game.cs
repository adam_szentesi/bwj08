using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
  public static Game Instance;
  public static Player Player { get; private set; }
  public static bool IsInLoop { get; private set; } = false;
  public static bool IsAnimating { get; private set; } = false;
  public RectInt LoopRect { get; private set; }

  public static Dictionary<Direction, Vector2Int> Offsets = new Dictionary<Direction, Vector2Int>
  {
    {Direction.Left,    new Vector2Int(-1, 0)},
    {Direction.Right,   new Vector2Int(1, 0)},
    {Direction.Up,      new Vector2Int(0, 1)},
    {Direction.Down,    new Vector2Int(0, -1)},
  };

  public static Dictionary<Direction, Direction> OppositeDirections = new Dictionary<Direction, Direction>
  {
    {Direction.Left,    Direction.Right},
    {Direction.Right,   Direction.Left},
    {Direction.Up,      Direction.Down},
    {Direction.Down,    Direction.Up},
  };

  [SerializeField]
  private int HeightRaidus = 1;

  [SerializeField]
  private TileManager TileManager;

  [SerializeField]
  private Palette RealmPalette;

  [SerializeField]
  private LoopCamera LoopCamera;

  [SerializeField]
  private MainCamera MainCamera;

  [SerializeField]
  private Color LoopBackgroundColor;

  [SerializeField]
  private LoopScreen LoopScreen;

  [SerializeField]
  private GameObject PlayerPrefab;

  [SerializeField]
  private float AnimationsDuration = 1.0f;

  [SerializeField]
  private LevelData[] LevelDatas = new LevelData[0];

  private InputActions InputAction;
  private int CurrentRealmIndex = 0;
  private Realm CurrentRealm;
  private int LoopRadius => CurrentRealm.LoopRadius;
  private int LoopSize => LoopRadius * 2 + 1;
  private int CurrentLevelIndex = 0;
  private Vector2 DefaultTextureOffset;
  private Vector2 DefaultTextureScale;

  private void Awake()
  {
    if (Instance != null)
    {
      Destroy(this);
      return;
    }

    Instance = this;

    InputAction = new InputActions();

    InputAction.Gameplay.Enable();
    InputAction.Gameplay.ToggleLoop.performed += context => ToggleLoop();
    InputAction.Gameplay.Switch.performed += context => IncreaseRealm();

    InputAction.Gameplay.MoveLeft.performed += context => CurrentRealm.OnMove(Direction.Left);
    InputAction.Gameplay.MoveRight.performed += context => CurrentRealm.OnMove(Direction.Right);
    InputAction.Gameplay.MoveUp.performed += context => CurrentRealm.OnMove(Direction.Up);
    InputAction.Gameplay.MoveDown.performed += context => CurrentRealm.OnMove(Direction.Down);
    InputAction.Gameplay.Action.performed += context => CurrentRealm.OnAction();
  }

  private void Start()
  {
    Player = Instantiate(PlayerPrefab, transform).GetComponent<Player>();
    Player.SetTilePosition(Vector2Int.zero);

    MainCamera.SetCameraSize(HeightRaidus + 0.5f);
    MainCamera.ToggleLayers(IsInLoop);

    LoadLevel();
    SwitchRealm();
    UpdateLoopScreenSize();
  }

  private void UpdateLoopScreenSize()
  {
    float screenRatio = (float)Screen.width / (float)Screen.height;

    float screenHeight = MainCamera.Size * 2;
    float screenWidth = screenRatio * screenHeight;

    LoopScreen.SetScale(screenWidth, screenHeight);
  }

  private void UpdateLoopCameraProperties()
  {
    LoopCamera.SetCameraSize(LoopRadius + 0.5f);

    float screenRatio = (float)Screen.width / (float)Screen.height;

    float textureHeight = MainCamera.Size / LoopCamera.Size;
    float textureWidth = screenRatio * textureHeight;

    DefaultTextureScale = new Vector2(textureWidth, textureHeight);
    DefaultTextureOffset = -DefaultTextureScale * 0.5f + new Vector2(0.5f, 0.5f);
  }

  private void LoadLevel()
  {
    TileManager.LoadLevel(LevelDatas[CurrentLevelIndex]);
  }

  private void ToggleLoop()
  {
    IsInLoop = !IsInLoop;

    MainCamera.ToggleLayers(IsInLoop);
    LoopCamera.Toggle(IsInLoop);
    UpdateLoopRect();

    if (IsInLoop)
    {
      LoopScreen.SetTextureScale(DefaultTextureScale);
      LoopScreen.SetTextureOffset(DefaultTextureOffset);
      LoopScreen.SetPosition(Player.TilePosition2D);

      LoopCamera.SetPosition(Player.TilePosition2D);
    }
  }

  private void IncreaseRealm()
  {
    CurrentRealmIndex++;

    if (CurrentRealmIndex > RealmPalette.Realms.Count - 1)
    {
      CurrentRealmIndex = 0;
    }

    SwitchRealm();
  }

  private void SwitchRealm()
  {
    CurrentRealm = RealmPalette.Realms[CurrentRealmIndex];

    UpdateLoopCameraProperties();
    UpdateColors();
    UpdateLoopRect();
  }

  private void UpdateColors()
  {
    MainCamera.SetForegroundColor(CurrentRealm.Color);
    MainCamera.SetBackgroundColor(LoopBackgroundColor);
  }

  private void UpdateLoopRect()
  {
    LoopRect = new RectInt
    (
      Player.TilePosition.x - LoopRadius,
      Player.TilePosition.y - LoopRadius,
      LoopSize,
      LoopSize
    );
  }

  public void MovePlayer(Direction direction)
  {
    Vector2Int target = Player.TilePosition2D + Offsets[direction];
    Vector2Int desiredPosition = IsInLoop ? LoopPosition(target) : target;

    if (TileManager.CanMoveTo(desiredPosition))
    {
      StartCoroutine(Animate(target, desiredPosition));
    }
  }

  private Vector2Int LoopPosition(Vector2Int position)
  {

    if (position.x >= LoopRect.xMax)
    {
      position.x -= LoopSize;
    }
    else if (position.x < LoopRect.xMin)
    {
      position.x += LoopSize;
    }

    if (position.y >= LoopRect.yMax)
    {
      position.y -= LoopSize;
    }
    else if (position.y < LoopRect.yMin)
    {
      position.y += LoopSize;
    }

    return position;
  }

  private IEnumerator Animate(Vector2 target, Vector2Int desiredPosition)
  {
    IsAnimating = true;
    InputAction.Gameplay.Disable();

    Vector2 origin = Player.TilePosition2D;

    float time = 0.0f;

    while (time <= AnimationsDuration)
    {
      time += Time.deltaTime;
      Vector2 position = Vector2.Lerp(origin, target, time / AnimationsDuration);
      SetPlayerPosition(position);
      yield return null;
    }

    SetPlayerTilePosition(desiredPosition);

    IsAnimating = false;
    InputAction.Gameplay.Enable();
  }

  private void SetPlayerPosition(Vector2 position)
  {
    Player.SetPosition(position);
    MainCamera.SetPosition(position);

    if (IsInLoop)
    {
      Vector2 materialOffset = position - LoopRect.min - new Vector2Int(LoopRadius, LoopRadius);
      LoopScreen.SetTextureOffset(materialOffset / Instance.LoopSize + DefaultTextureOffset);
      LoopScreen.SetPosition(position);
    }
  }

  private void SetPlayerTilePosition(Vector2Int tilePosition)
  {
    Player.SetTilePosition(tilePosition);
    MainCamera.SetPosition(Player.TilePosition2D);

    if (IsInLoop)
    {
      LoopScreen.SetPosition(tilePosition);
    }
  }

  public static Vector3Int GetOffset(Direction direction)
  {
    Vector2Int offset = Offsets[direction];
    return new Vector3Int(offset.x, offset.y, 0);
  }

  public static Sprite GetTileSprite(TileType tileType) => Instance.TileManager.GetTileSprite(tileType);

}
