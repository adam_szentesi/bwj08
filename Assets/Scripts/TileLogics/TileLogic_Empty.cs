using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/TileLogic_Empty")]
public class TileLogic_Empty : TileLogic
{
  public override bool CanMoveTo()
  {
    return true;
  }

}
