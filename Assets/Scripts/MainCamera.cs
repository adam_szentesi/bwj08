using UnityEngine;

public class MainCamera : CameraBase
{
  [SerializeField]
  private Shader RealmShader;

  private Material RealmMaterial;

    protected override void Awake()
    {
        base.Awake();

        RealmMaterial = new Material(RealmShader);
    }

  private void OnRenderImage(RenderTexture source, RenderTexture destination)
  {
    Graphics.Blit(source, destination, RealmMaterial);
  }

  public void SetForegroundColor(Color color)
  {
    RealmMaterial.SetColor("_ForegroundColor", color);
  }

  public void SetBackgroundColor(Color color)
  {
    RealmMaterial.SetColor("_BackgroundColor", color);
  }

    public void ToggleLayers(bool isInLoop)
    {
        int layerMask = isInLoop ? LayerMask.GetMask("Loop") : LayerMask.GetMask("Realm");
        Camera.cullingMask = layerMask;
    }
    

}
