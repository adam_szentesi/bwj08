using UnityEngine;

public class LoopCamera : CameraBase
{
    protected override void Awake()
    {
        base.Awake();

        Toggle(false);
    }

    public void Toggle(bool state)
    {
        Camera.enabled = state;
    }

}
