using System;
using System.Collections.ObjectModel;
using UnityEngine;

[CreateAssetMenu(menuName = "BWJ/LevelData")]
public class LevelData : ScriptableObject
{
  public RectInt Size;

  [SerializeField]
  private TileSaveData[] TileSaveDatas = new TileSaveData[0];

  public ReadOnlyCollection<TileSaveData> GetData()
  {
    return Array.AsReadOnly<TileSaveData>(TileSaveDatas);
  }

}
