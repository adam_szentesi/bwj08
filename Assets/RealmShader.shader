Shader "BWJ/RealmShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ForegroundColor("Foreground color", Color) = (1.0, 1.0, 1.0, 1.0)
        _BackgroundColor("Background color", Color) = (0.0, 0.0, 0.0, 1.0)
    }

    SubShader
    {
        Cull    Off
        ZWrite  Off
        ZTest   Always

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct u2v
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert (u2v v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _ForegroundColor;
            float4 _BackgroundColor;

            fixed4 frag(v2f i) : SV_Target
            {
                float value = tex2D(_MainTex, i.uv).r;

                return value * _ForegroundColor + (1 - value) * _BackgroundColor;
            }

            ENDCG
        }
    }
}
